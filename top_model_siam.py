from unittest.mock import _ANY

from keras.applications import ResNet50
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, Model
from keras.layers import Dropout, Flatten, Dense, Reshape, Input, Concatenate, Subtract
from keras import applications
from keras import optimizers
from keras import backend as K
import tensorflow as tf
from keras import losses

import numpy as np
import os.path
from sklearn.metrics import confusion_matrix, mean_squared_error

import argparse





# CONFIGURATION #
parser = argparse.ArgumentParser(description='Build a top layer for the similarity training and train it.')
parser.add_argument('-w', '--overwrite', dest="w",
                    action='store_true', help="Overwrite initial feature embeddings")
parser.add_argument('-t', '--train-dir', dest='train_dir',
                    default="data/siamese/mixed/",
                    help='Path to dataset training directory')
parser.add_argument('-v', '--valid-dir', dest='valid_dir',
                    default="data/siamese/mixedvalid/",
                    help='Path to dataset validation directory')
parser.add_argument('-e', '--epochs', dest='epochs', type=int,
                    default=30, help='Number of epochs to train the model')
parser.add_argument('-b', '--batch-size', dest='batch_size', type=int,
                    default=2, help='Batch size')

args = parser.parse_args()


# img_width, img_height = 150, 150
# vgg16 input size 224, 224
img_width, img_height = 224, 224
top_model_weights_path = 'top_model_weights.h5'
train_data_dir = args.train_dir
valid_data_dir = args.valid_dir
train_features_file = 'top_model_features_train.npy'
valid_features_file = 'top_model_features_valid.npy'
nb_train_samples = len(os.listdir(train_data_dir + "/0")) / 2
nb_valid_samples = len(os.listdir(valid_data_dir + "/0")) / 2

epochs = args.epochs
batch_size = args.batch_size

resnet_model = applications.ResNet50(include_top=False, weights='imagenet')

layersTrainable = False

def save_features():
  datagen = ImageDataGenerator(rescale = 1. / 255)

  #vgg16_model = applications.VGG16(include_top = False, weights = 'imagenet')
  resnet_model = applications.ResNet50(include_top = False, weights = 'imagenet')
  # Attention in generator width is swapped with height
  generator = datagen.flow_from_directory(train_data_dir, target_size = (img_height, img_width), batch_size = batch_size, class_mode = None, shuffle = False)
  top_model_features_train = resnet_model.predict_generator(generator, nb_train_samples)
  top_model_features_train = top_model_features_train.reshape(-1,2)
  # top_model_features_train = vgg16_model.predict_generator(generator, nb_train_samples // batch_size)
  np.save(open(train_features_file, 'wb'), top_model_features_train)

  generator = datagen.flow_from_directory(valid_data_dir, target_size = (img_height, img_width), batch_size = batch_size, class_mode = None, shuffle = False)
  # Kam antra karta dalinama is triju man nesuprantama
  # top_model_features_valid = vgg16_model.predict_generator(generator, nb_valid_samples // batch_size)
  top_model_features_valid = resnet_model.predict_generator(generator, nb_valid_samples)
  top_model_features_valid = top_model_features_valid.reshape(-1,2)
  np.save(open(valid_features_file, 'wb'), top_model_features_valid)

#papildyt is popieriaus
def contrastive_loss(y_true, y_pred):
    '''Contrastive loss from Hadsell-et-al.'06
    http://yann.lecun.com/exdb/publis/pdf/hadsell-chopra-lecun-06.pdf
    '''
    embeddings = K.reshape(y_pred, (-1, 2))
    dist = np.linalg.norm(embeddings[:0] - embeddings[:1])

    margin = 1
    #return K.mean(y_true * K.square(y_pred) + (1 - y_true) * K.square(K.maximum(margin - y_pred, 0)))
    return K.mean(y_true * K.square(dist) + (1 - y_true) * K.square(K.maximum(margin - dist, 0)))

def init_base_model(output_dims=1):
    in_dims = (None, None, 3)

    # Create the 2 inputs
    # input size should be 224x224x3
    #item_1 = Input(shape=in_dims)
    #item_2 = Input(shape=in_dims)

    #embeddings = K.reshape(Input(in_dims), (-1, 2))
    #item_1 = embeddings[:0]
    #item_2 = embeddings[:1]

    item_1 = Input(in_dims)[:0]
    item_2 = Input(in_dims)[:1]

    for layer in resnet_model.layers:
        layer.trainable = layersTrainable

    base_model = Model(inputs=resnet_model.inputs, outputs=resnet_model.outputs)

    resnet_output1 = base_model(item_1)
    resnet_output2 = base_model(item_2)

    item1_output = Reshape((25088,))(resnet_output1)
    item2_output = Reshape((25088,))(resnet_output2)

    concatinatedResult = Concatenate(axis=-1)([item1_output, item2_output])

    top_fc1 = Dense(64, activation='relu')(concatinatedResult)
    top_fc2 = Dropout(0.5)(top_fc1)
    top_preds = Dense(output_dims, activation='sigmoid')(top_fc2)

    top_model = Model(inputs=[item_1, item_2], outputs=top_preds)
    return top_model

def init_base_model2(output_dims=1):
    in_dims = (None, None, 3)
    item_1 = Input(shape=in_dims)#[0]
    item_2 = Input(shape=in_dims)#[1]

    for layer in resnet_model.layers:
        layer.trainable = layersTrainable

    base_model = Model(inputs=resnet_model.inputs, outputs=resnet_model.outputs)

    resnet_output1 = base_model(item_1)
    resnet_output2 = base_model(item_2)

    item1_output = Reshape((25088,))(resnet_output1)
    item2_output = Reshape((25088,))(resnet_output2)

    top_fc1_1 = Dense(64, activation='relu')(item1_output)
    top_fc1_2 = Dense(64, activation='relu')(item2_output)

    top_fc2_1 = Dropout(0.5)(top_fc1_1)
    top_fc2_2 = Dropout(0.5)(top_fc1_2)

    concatinatedResult = Concatenate(axis=-1)([top_fc2_1, top_fc2_2])

    top_preds = Dense(output_dims, activation='sigmoid')(concatinatedResult)

    top_model = Model(inputs=[item_1, item_2], outputs=top_preds)
    return top_model

def train_model():
    top_model = init_base_model2()


    train_data = np.load(open('top_model_features_train.npy', 'rb'))
    train_labels = np.array(open("data\siamese\mixedlabels.txt", "r").readline().split(sep=","))

    valid_data = np.load(open('top_model_features_valid.npy', 'rb'))
    valid_labels = np.array(open("data\siamese\mixedvalidlabels.txt", "r").readline().split(sep=","))

    top_model.compile(optimizer=optimizers.Adam, loss=contrastive_loss, metrics=['accuracy', 'mse'])
    #top_model.fit()
    top_model.fit(train_data, train_labels, epochs=epochs, batch_size=batch_size * 11, shuffle=True)
    top_model.save_weights(top_model_weights_path)

    pred_valid = top_model.predict(valid_data, batch_size=32)
    print(contrastive_loss(valid_labels, pred_valid))
    print(train_data.shape)
    print(train_labels)
    print(pred_valid[0:9], pred_valid.shape)
    print('Validation MSE', mean_squared_error(valid_labels, pred_valid))
    print(valid_data.shape)

# MAIN #
if nb_train_samples > 0 and nb_valid_samples > 0:
  if args.w or not (os.path.isfile(train_features_file) and os.path.isfile(valid_features_file)):
    print("Writing features")
    save_features()
  train_model()
else:
  print("Dataset images were not found")